<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['product_id', 'voucher_id', 'name', 'business', 'phone', 'email', 'address']; 

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function voucher()
    {
        return $this->belongsTo('App\Voucher');
    }
}
