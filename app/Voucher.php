<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voucher extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'amount', 'images', 'desc', 'start', 'end'];
    protected $dates = ['deleted_at'];

    public function orders()
    {
        return $this->hasMany('App\Order');
    }
}
