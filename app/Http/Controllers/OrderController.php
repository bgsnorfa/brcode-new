<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Voucher;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::paginate(15);
        
        return view('order.index', [
            'orders' => $orders
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($slug)
    {
        $product = Product::where('slug', '=', $slug)->firstOrFail();

        return view('order', [
            'product' => $product
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'business' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email',
            'address' => 'required',
            'voucher_id' => 'nullable|exists:vouchers,name'
        ]);
        
        $voucher_id = Voucher::where('name', $request->voucher_id)->first();
        if (!$voucher_id == null) {
            $request->merge(['voucher_id' => $voucher_id->id]);
        }
        
        // dd($request->only(['product_id', 'voucher_id', 'name', 'business', 'phone', 'email', 'address']));
        $order = Order::create($request->only(['product_id', 'voucher_id', 'name', 'business', 'phone', 'email', 'address']));

        return redirect('order-success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function success()
    {
        return view('order-success');
    }

    public function isDone($id)
    {
        $order = Order::find($id)->first();
        $order->done = 1;
        $order->save();

        return redirect('order')->with('message', 'Order telah ditandai selesai!');
    }
}
