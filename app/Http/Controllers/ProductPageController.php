<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductPageController extends Controller
{
    public function cv()
    {
        $cv = Product::where('category', '=', 'cv')
            ->paginate(8);

        return view('product.cv')
            ->with('title', 'Personal Resume')
            ->with('cv', $cv);
    }

    public function cvSlug($slug)
    {
        $data = Product::where('slug', '=', $slug)->first();
        return view('product.preview')
            ->with('title', $data->title)
            ->with('data', $data);
    }

    public function blog()
    {
        $blog = Product::where('category', '=', 'blog')
            ->paginate(8);

        return view('product.blog')
            ->with('title', 'Blog')
            ->with('blog', $blog);
    }

    public function blogSlug($slug)
    {
        $data = Product::where('slug', '=', $slug)->first();
        return view('product.preview')
            ->with('title', $data->title)
            ->with('data', $data);
    }

    public function business()
    {
        $business = Product::where('category', '=', 'business')
            ->paginate(8);

        return view('product.business')
            ->with('title', 'Corporate Business')
            ->with('business', $business);
    }

    public function businessSlug($slug)
    {
        $data = Product::where('slug', '=', $slug)->first();
        return view('product.preview')
            ->with('title', $data->title)
            ->with('data', $data);
    }
}
