<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Product;
use File;
use Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('save')) {
            $this->validate($request, [
                'title' => 'required',
                'image' => 'required|image',
                'link' => 'required',
                'price' => 'required',
                'by' => 'required',
                'by_url' => 'required'
            ]);

            $p = new Product;
            $p->title = $request->title;
            $p->category = $request->category;
            $p->link = $request->link;
            $p->price = $request->price;
            $p->slug = str_slug($request->title);
            $p->desc = $request->desc;
            $p->by = $request->by;
            $p->by_url = $request->by_url;

            $ext = Input::file('image')->getClientOriginalExtension();
            $filename = 'p-' . $request->category . '-' . Input::file('image')->getClientOriginalName();
            $request->file('image')->move('images/p/', $filename);

            $p->image = $filename;
            $p->save();

            return view('home')->with('desc', $request->desc);
        }

        return view('home');
    }
}
