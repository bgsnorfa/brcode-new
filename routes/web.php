<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'HomeController@index');

Route::get('/', 'FrontPageController@index');

Route::get('/market', 'FrontPageController@index');
Route::get('/tentang-kami', 'FrontPageController@about');
Route::get('/faq', 'FrontPageController@faq');
// Route::get('/produk', 'FrontPageController@product');

// route for product
Route::get('/cv', 'ProductPageController@cv');
Route::get('/cv/{slug}', 'ProductPageController@cvSlug');
Route::get('/blog', 'ProductPageController@blog');
Route::get('/blog/{slug}', 'ProductPageController@blogSlug');
Route::get('/business', 'ProductPageController@business');
Route::get('/business/{slug}', 'ProductPageController@businessSlug');

// route for order
Route::get('order', 'OrderController@index')->middleware('auth');
Route::get('order/done/{id}', 'OrderController@isDone')->middleware('auth');
Route::get('order/{slug}', 'OrderController@create');
Route::post('order/{slug}', 'OrderController@store');
Route::get('order-success', 'OrderController@success');

// route for voucher
Route::get('vouchers', 'VoucherController@index');
Route::get('voucher', 'VoucherController@all');
Route::get('voucher/show/{id}', 'VoucherController@show');
Route::get('voucher/create', 'VoucherController@create');
Route::post('voucher/create', 'VoucherController@store');
Route::get('voucher/delete/{id}', 'VoucherController@destroy');
