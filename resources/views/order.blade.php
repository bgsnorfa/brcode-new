@extends('layouts.front')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <div class="card card-order">
                <div class="card-header">
                    Order
                </div>
                <div class="card-body">
                    <p class="card-text text-info">Silahkan mengisi form berikut untuk melanjutkan pemesanan.</p>
                    <form method="post">
                        {{ csrf_field() }}
                        
                        <input type="hidden" name="product_id" value="{{ $product->id }}">

                        <div class="form-group">
                            <label for="name">Nama</label>
                            <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ old('name') }}">
                            {!! $errors->first('name', '<div class="invalid-feedback">:message</div>') !!}
                        </div>
                        <div class="form-group">
                            <label for="business">Nama Usaha</label>
                            <input type="text" name="business" class="form-control {{ $errors->has('business') ? 'is-invalid' : '' }}" value="{{ old('business') }}">
                            {!! $errors->first('business', '<div class="invalid-feedback">:message</div>') !!}
                        </div>
                        <div class="form-group">
                            <label for="phone">No HP</label>
                            <input type="text" name="phone" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" value="{{ old('phone') }}">
                            {!! $errors->first('phone', '<div class="invalid-feedback">:message</div>') !!}
                        </div>
                        <div class="form-group">
                            <label for="email">Alamat Email</label>
                            <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ old('email') }}">
                            {!! $errors->first('email', '<div class="invalid-feedback">:message</div>') !!}
                        </div>
                        <div class="form-group">
                            <label for="address">Alamat</label>
                            <textarea class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" name="address" rows="2">{{ old('address') }}</textarea>
                            {!! $errors->first('address', '<div class="invalid-feedback">:message</div>') !!}
                        </div>
                        <hr>
                        <div class="form-group">
                            <label for="voucher_id">Voucher</label>
                            <input type="text" class="form-control {{ $errors->has('voucher_id') ? 'is-invalid' : '' }}" name="voucher_id" value="{{ old('voucher_id') }}">
                            {!! $errors->first('voucher_id', '<div class="invalid-feedback">:message</div>') !!}
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card card-order">
                <img class="card-img-top" src="{{ asset('/images/p/' . $product->image ) }}" alt="{{ $product->title }}">
                <div class="card-body">
                    <h4 class="card-title">{{ $product->title }}</h4>
                    <p class="card-text">{{ $product->price }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection