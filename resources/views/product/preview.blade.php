@extends('layouts.front')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 text-center">
            <br/>
            <h1 style="text-align: center">{{ $data->title }}</h1>
            <br/>
            <img src="{{ asset('/images/p/' . $data->image ) }}" class="img-fluid"/>
            <br/>
            <br/>
            <a class="btn btn-success" target="_blank" href="{{ $data->link }}">Live Preview</a>
            {{--  <a class="btn btn-success" target="_blank" href="http://bit.ly/order-brcode">Order Now</a>  --}}
            <a href="{{ url('order', $data->slug) }}" class="btn btn-success">Order Now</a>
            <br/>
            <br/>
        </div>

        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            Harga pembuatan (Backend dan implementasi) : IDR {{ $data->price }}
            <br/><br/>
            {!! $data->desc !!}
            <br/><br/>
            Didesain oleh : <a style="color: #4d4d4d" class="product-link" href="{{ $data->by_url }}">{{ $data->by }}</a>
        </div>
        <div class="col-sm-2"></div>

    </div>
</div>
@endsection
