@extends('layouts.front')

@section('content')
<div class="section section-product-page paralax">
    <div class="container">
        <div class="row">
            <div class="col-12 hero-text text-center">
                <h1>Personal Resume</h1>
                <h5>Sesuaikan kebutuhan anda dengan berbagai pilihan tema yang telah kami sediakan</h5>
            </div>
        </div>
    </div>
</div>

<div class="section container">
    <div class="row" style="margin-bottom: 3em;">
        <div class="col-12 text-center">
            <h3 style="color: grey;">Personal Resume (Curriculum Vitae)</h3>
        </div>
        @foreach($cv as $data)
        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="cv/{{ $data->slug }}">
                    <img src="images/p/{{ $data->image }}" class="img-fluid"/>
                    <div class="price-topleft-blue">
                        <p>IDR {{ $data->price }}</p>
                    </div>
                </a>
            </div>
            <div class="text-center">
                <a href="cv/{{ $data->slug }}" class="product-link">
                    <h6><b>{{ $data->title }}</b></h6>
                </a>
                <p>By <a href="{{ $data->by_url }}" class="product-link">{{ $data->by }}</a></p>
            </div>
        </div>

        @endforeach

        <div style="margin: 0 auto; padding-top: 2em; clear: both;">
            {{ $cv->links('vendor.pagination.bootstrap-4') }}
        </div>

    </div>
</div>
@endsection
