@extends('layouts.front')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card card-order">
                <div class="card-body">
                    <h4 class="card-title">Selamat!</h4>
                    <p class="card-text">Pesanan anda berhasil diproses. Terima kasih telah memberikan kepercayaan pada kami untuk website anda.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection