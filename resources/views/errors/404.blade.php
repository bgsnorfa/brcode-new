@extends('layouts.front')

@section('content')
<br/><br/>
<h1 style="font-size: 700%; text-align: center; font-weight: bold; color: #4f4f4f">404</h1>
<h2 style="font-size: 300%; text-align: center; font-weight: bold; color: #4f4f4f;">NOT FOUND</h2>

<div style="background-color: #dfdfdf; padding-top: 5px; padding-bottom: 5px;">
  <div class="container">
    <div class="row">
      <a class="btn" href="{{ url('/') }}"><i class="fa fa-chevron-left"></i> Kembali ke Halaman Utama</a>
    </div>
  </div>
</div>
@endsection
