@extends('layouts.front')

@section('content')
<div class="section section-quote paralax">
    <div class="container">
        <div class="row">
            <div class="col-12 hreo-text text-center">
                <h1>Marketplace Produk Website</h1>
                <br/>
                <h5>Jasa Pembuatan Website Personal Resume, Blog, dan Corporate Business</h5>
                <br/>
            </div>

            <!--<div class="col-sm-2"></div>-->
            <!--<div class="col-sm-8">-->
            <!--    <h5 style="text-align: center"><b>Cari Apapaun</b></h5>-->
            <!--    <br/>-->
            <!--    <form method="get" action="">-->
            <!--        <div class="input-group stylish-input-group">-->
            <!--            <input type="text" class="form-control"  placeholder="Cari fitur, template, atau kategori website" >-->
            <!--            <span class="input-group-addon">-->
            <!--                <button type="submit">-->
            <!--                    <span class="fa fa-search"></span>-->
            <!--                </button>-->
            <!--            </span>-->
            <!--        </div>-->
            <!--    </form>-->
            <!--</div>-->
            <!--<div class="col-sm-2"></div>-->

        </div>
    </div>
</div>

<!-- produk -->
<div class="section-product">
  <div class="container">
    <h2 class="text-center">
      Produk yang kami sediakan
    </h2>
    <br/>

    <div class="row">
      <div class="col-6 text-center computer-bg">
        <img src="{{ asset('images/computer-blue3.png') }}" class="img-fluid"/>
      </div>

      <div class="col-md-2 text-center">
        <img src="{{ asset('images/coprol3.png') }}" class="fade img-fluid"/>
        <br/>
        <br/>
        <h5><b>Blog</b></h5>
        <!-- <p>
          lorem ipsum
        </p> -->
      </div>

      <div class="col-md-2 text-center">
        <img src="{{ asset('images/cv3.png') }}" class="fade img-fluid"/>
        <br/>
        <br/>
        <h5><b>Personal Resume</b></h5>
        <!-- <p>
          lorem ipsum
        </p> -->
      </div>

      <div class="col-md-2 text-center">
        <img src="{{ asset('images/org3.png') }}" class="fade img-fluid"/>
        <br/>
        <br/>
        <h5><b>Corporate Business</b></h5>
        <!-- <p>
          lorem ipsum
        </p> -->
      </div>
    </div>
  </div>
</div>

<!-- featured produk grid  -->
<div class="section container">
    <div class="row">
        <div class="col-12 text-center">
            <h2>Temukan Berbagai Tampilan yang Sesuai dengan Kebutuhan Anda </h2>
        </div>
    </div>
    <br/><br/><br/>

    <!-- cv -->
    <div class="row" style="margin-bottom: 3em;">
        <div class="col-12 text-center">
            <h3 style="color: grey;">Personal Resume (Curriculum Vitae)</h3>
        </div>
        @foreach($cv as $data)
        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="cv/{{ $data->slug }}">
                    <img src="images/p/{{ $data->image }}" class="img-fluid"/>
                    <div class="price-topleft-blue">
                        <p>IDR {{ $data->price }}</p>
                    </div>
                </a>
            </div>
            <div class="text-center">
                <a href="cv/{{ $data->slug }}" class="product-link">
                    <h6><b>{{ $data->title }}</b></h6>
                </a>
                {{--  <p>By <a href="{{ $data->by_url }}" class="product-link">{{ $data->by }}</a></p>  --}}
            </div>
        </div>

        @endforeach

        <div class="col-12 text-center">
            <a class="btn btn-info" href="{{ url('/cv') }}">Lihat tampilan CV Selengkapnya <i class="fa fa-chevron-right"></i></a>
        </div>

    </div>

    <!-- blog -->
    <div class="row" style="margin-bottom: 3em;">
        <div class="col-12 text-center">
            <h2 style="color: grey;">Blog</h2>
        </div>

        @foreach($blog as $data)
        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="blog/{{ $data->slug }}">
                    <img src="images/p/{{ $data->image }}" class="img-fluid"/>
                    <div class="price-topleft-green">
                        <p>IDR {{ $data->price }}</p>
                    </div>
                </a>
            </div>

            <div class="text-center">
                <a href="blog/{{ $data->slug }}" class="product-link">
                    <h6><b>{{ $data->title }}</b></h6>
                </a>
                {{--  <p>By <a href="{{ $data->by_url }}" class="product-link">{{ $data->by }}</a></p>  --}}
            </div>
        </div>
        @endforeach
        <div class="col-12 text-center">
            <a class="btn btn-info" href="{{ url('/blog') }}">Lihat tampilan Blog Selengkapnya <i class="fa fa-chevron-right"></i></a>
        </div>

    </div>

    <!-- corporate business -->
    <div class="row" style="margin-bottom: 3em;">
        <div class="col-12 text-center">
            <h2 style="color: grey;">Corporate Business</h2>
        </div>
        @foreach($business as $data)
        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="business/{{ $data->slug }}">
                    <img src="images/p/{{ $data->image }}" class="img-fluid"/>
                    <div class="price-topleft-red">
                        <p>IDR {{ $data->price }}</p>
                    </div>
                </a>
            </div>

            <div class="text-center">
                <a href="business/{{ $data->slug }}" class="product-link">
                    <h4>{{ $data->title }}</h4>
                </a>
                {{--  <p>By <a href="{{ $data->by_url }}" class="product-link">{{ $data->by }}</a></p>  --}}
            </div>
        </div>
        @endforeach

        <div class="col-12 text-center">
            <a class="btn btn-info" href="{{ url('/business') }}">Lihat tampilan Korporasi Selengkapnya <i class="fa fa-chevron-right"></i></a>
        </div>

    </div>

</div>


<!-- order method -->
<div class="section section-howto paralax">
  <div class="container">
    <h2 class="text-center">
      Cara Pesan
    </h2>
    <br/>
    <div class="row">
      <div class="col-sm-4 text-center">
        <h4>Pilih produk dan tema</h4><br/>
        <img src="{{ asset('images/list.png') }}" class="grow"/>
        <br/><br/>
        <p class="slidedown">Pilih produk yang akan dibuat, dan tentukan tema yang sesuai kebutuhan dengan melihat katalog produk kami.</p>
      </div>

      <div class="col-sm-4 text-center">
        <h4>Ajukan permintaan</h4><br/>
        <img src="{{ asset('images/forms.png') }}" class="grow"/>
        <br/><br/>
        <p class="slidedown">Tetapkan pilihan produk dan tema, lalu ajukan permintaan.</p>
      </div>

      <div class="col-sm-4 text-center">
        <h4>Buat kesepakatan</h4><br/>
        <img src="{{ asset('images/hand-shake.png') }}" class="grow"/>
        <br/><br/>
        <p class="slidedown">Setelah data kami terima, kami akan segera menghubungi anda dan website kami kerjakan</p>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12 text-center">
        <br/>
        <a class="btn btn-lg btn-info" href="http://bit.ly/order-brcode">
          <i class="fa fa-chevron-right"></i> Pesan Sekarang
        </a>
      </div>
    </div>
  </div>
</div>

<!-- recommend -->
<div class="section section-recommend">
  <div class="container">
    <h2 class="text-center">
      Mereka Merekomendasikan BR | Code
    </h2>
    <br/>
    <div class="col-sm-4">
      <img src="{{ asset('images/sandy.jpg') }}" class="img-fluid customer-img"/>
      <blockquote>
        I am quite happy for the results of work from BRCode. They offered many interesting models of web products, with friendly prices and short amount of time in development process when making my personal resume
        <a href="http://alfonsussandy.id">alfonsussandy.id</a>
        <br/>
        <cite>- Alfonsus Sandy, Diporental</cite>
      </blockquote>
    </div>
  </div>
</div>

<div class="modal" id="promo">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <a href="{{ url('voucher') }}">
            <img src="{{ asset('images/disk.jpg') }}" class="img-fluid rounded">
        </a>
    </div>
  </div>
</div>
@endsection

@section('bottom-script')
<script type="text/javascript">
$(document).ready(function () {

    $('#promo').modal('show');

});
</script>
@endsection