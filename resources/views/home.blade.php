@extends('layouts.app')

@section('content')
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                    <br/>
                    <form method="post" action="{{ url()->current() }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label for="input">Title</label>
                            <input type="text" name="title" value="{{ old('title') }}" class="form-control">
                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group">
                            <label for="input">Category</label>
                            <select name="category" class="form-control" required="required">
                                <option value="">--- Choose Category ---</option>
                                <option value="cv">CV</option>
                                <option value="blog">Blog</option>
                                <option value="business">Corporate Business</option>
                            </select>
                        </div>

                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                            <label for="input">Image</label>
                            <input type="file" name="image" class="form-control">
                            {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('link') ? 'has-error' : '' }}">
                            <label for="input">Link</label>
                            <input type="text" name="link" value="{{ old('link') }}" class="form-control">
                            {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
                            <label for="input">Price</label>
                            <input type="text" name="price" value="{{ old('price') }}" class="form-control">
                            {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group">
                            <label for="input">Description</label>
                            <textarea name="desc">{{ old('desc') }}</textarea>
                        </div>

                        <div class="form-group {{ $errors->has('by') ? 'has-error' : '' }}">
                            <label for="input">Theme creator</label>
                            <input type="text" name="by" value="{{ old('by') }}" class="form-control">
                            {!! $errors->first('by', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('by_url') ? 'has-error' : '' }}">
                            <label for="input">Theme creator URL</label>
                            <input type="text" name="by_url" value="{{ old('by_url') }}" class="form-control">
                            {!! $errors->first('by_url', '<p class="help-block">:message</p>') !!}
                        </div>

                        <input type="submit" name="save" class="btn btn-success" value="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
