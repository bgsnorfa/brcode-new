@extends('layouts.front')

@section('content')
<!-- hero section -->
<div class="" style=" height: 500px;">
  <div class="paralax hero-image">
    <div class="hero-text">
      <h1>
        BR | Code
      </h1>
      <hr/>
      <h3>Marketplace Produk Digital</h3>
      <p>Pembuatan Website Company Profile, Organisasi, Personal Resume (CV)</p>
      <br/>
      <a class="btn btn-lg btn-animate" href="http://bit.ly/order-brcode"><i class="fa fa-chevron-right"></i> Ajukan Permintaan</a>
    </div>
  </div>
</div>

<div class="section section-why">
  <div class="container">
    <h2 class="text-center">
      Kenapa Pilih Kami
    </h2>

    <br/>

    <div class="row">
      <div class="col-xs-6 col-6 col-sm-3 col-md-3">
        <div class="text-center fa-why">
          <i class="fa fa-money"></i>
        </div>
        <h4 class="text-center">Cost Effective</h4>
        <p class="text-center">
          Kami memberikan harga terbaik
        </p>
      </div>

      <div class="col-6 col-xs-6 col-sm-3 col-md-3">
        <div class="text-center fa-why">
          <i class="fa fa-user-o"></i>
        </div>
        <h4 class="text-center">User Friendly</h4>
        <p class="text-center">Kami menggunakan desain terbaik yang mudah dioperasikan</p>
      </div>

      <div class="col-6 col-xs-6 col-sm-3 col-md-3">
        <div class="text-center fa-why">
          <i class="fa fa-mobile"></i>
        </div>
        <h4 class="text-center">Mobile Friendly</h4>
        <p class="text-center">Kami menerapkan tampilan responsif yang dapat diakses perangkat apapun</p>
      </div>

      <div class="col-6 col-xs-6 col-sm-3 col-md-3">
        <div class="text-center fa-why">
          <i class="fa fa-star-o"></i>
        </div>
        <h4 class="text-center">Experienced Developer</h4>
        <p class="text-center">Tak perlu diragukan lagi, kami telah pengalaman di bidang website</p>
      </div>

    </div>

    <div class="row">
      <div class="col-6 col-xs-6 col-sm-3 col-md-3">
        <div class="text-center fa-why">
          <i class="fa fa-rocket"></i>
        </div>
        <h4 class="text-center">High Performance</h4>
        <p class="text-center">Kami menghasilkan website dengan performa terbaik</p>
      </div>

      <div class="col-6 col-xs-6 col-sm-3 col-md-3">
        <div class="text-center fa-why">
          <i class="fa fa-phone"></i>
        </div>
        <h4 class="text-center">Customer Support</h4>
        <p class="text-center">Konsultasikan dengan kami apa yang anda butuhkan sekarang juga</p>
      </div>

      <div class="col-6 col-xs-6 col-sm-3 col-md-3">
        <div class="text-center fa-why">
          <i class="fa fa-file-pdf-o"></i>
        </div>
        <h4 class="text-center">Manual Guide Available</h4>
        <p class="text-center">Tak perlu khawatir, kami menyertakan panduan penggunaan</p>
      </div>

      <div class="col-6 col-xs-6 col-sm-3 col-md-3">
        <div class="text-center fa-why">
          <i class="fa fa-thumbs-o-up"></i>
        </div>
        <h4 class="text-center">Granted Quality</h4>
        <p class="text-center">Sudah tidak diragukan lagi, kami memberikan kualitas terbaik</p>
      </div>

    </div>

  </div>
</div>

<div class="section section-quote paralax">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="text-center">
          <i class="fa fa-quote-left"></i>
            <i>
                <span class="col-12 span-brand-1">Berikan Presentasi Menarik,</span>
                <br/><br/>
                <span class="col-12 span-brand-2">Bangun Branding Terbaik</span>
            </i>
          <i class="fa fa-quote-right"></i>
        </h3>
      </div>
    </div>
  </div>
</div>

<div class="section-product">
  <div class="container">
    <h2 class="text-center">
      Produk
    </h2>
    <br/>

    <div class="row">
      <div class="col-6 text-center computer-bg">
        <img src="{{ asset('images/computer-blue3.png') }}" class="img-fluid"/>
      </div>

      <div class="col-md-2 text-center">
        <img src="{{ asset('images/coprol3.png') }}" class="fade img-fluid"/>
        <br/>
        <br/>
        <h5>Blog</h5>
        <!-- <p>
          lorem ipsum
        </p> -->
      </div>

      <div class="col-md-2 text-center">
        <img src="{{ asset('images/cv3.png') }}" class="fade img-fluid"/>
        <br/>
        <br/>
        <h5>Personal Resume</h5>
        <!-- <p>
          lorem ipsum
        </p> -->
      </div>

      <div class="col-md-2 text-center">
        <img src="{{ asset('images/org3.png') }}" class="fade img-fluid"/>
        <br/>
        <br/>
        <h5>Corporate Business</h5>
        <!-- <p>
          lorem ipsum
        </p> -->
      </div>
    </div>
  </div>
</div>

<div class="section section-howto paralax">
  <div class="container">
    <h2 class="text-center">
      Cara Pesan
    </h2>
    <br/>
    <div class="row">
      <div class="col-sm-4 text-center">
        <h4>Pilih produk dan tema</h4><br/>
        <img src="{{ asset('images/list.png') }}" class="grow"/>
        <br/><br/>
        <p class="slidedown">Pilih produk yang akan dibuat, dan tentukan tema yang sesuai kebutuhan dengan melihat katalog produk kami.</p>
      </div>

      <div class="col-sm-4 text-center">
        <h4>Ajukan permintaan</h4><br/>
        <img src="{{ asset('images/forms.png') }}" class="grow"/>
        <br/><br/>
        <p class="slidedown">Tetapkan pilihan produk dan tema, lalu ajukan permintaan.</p>
      </div>

      <div class="col-sm-4 text-center">
        <h4>Buat kesepakatan</h4><br/>
        <img src="{{ asset('images/hand-shake.png') }}" class="grow"/>
        <br/><br/>
        <p class="slidedown">Setelah data kami terima, kami akan segera menghubungi anda dan website kami kerjakan</p>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12 text-center">
        <br/>
        <a class="btn btn-lg btn-info" href="http://bit.ly/order-brcode">
          <i class="fa fa-chevron-right"></i> Pesan Sekarang
        </a>
      </div>
    </div>
  </div>
</div>

<div class="section section-stat">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 text-center">
        <h1>9</h1>
        <p>Klien telah bekerja sama dengan kami</p>
      </div>

      <div class="col-sm-6 text-center">
        <h1>20</h1>
        <p>Produk digital telah kami hasilkan</p>
      </div>

      <!-- <div class="col-sm-4 text-center">
        <h1></h1>
        <p></p>
      </div> -->

    </div>

  </div>
</div>

<div class="section section-recommend">
  <div class="container">
    <h2 class="text-center">
      Mereka Merekomendasikan BR | Code
    </h2>
    <br/>
    <div class="col-sm-4">
      <img src="{{ asset('images/sandy.jpg') }}" class="img-fluid customer-img"/>
      <blockquote>
        I am quite happy for the results of work from BRCode. They offered many interesting models of web products, with friendly prices and short amount of time in development process when making my personal resume
        <a href="http://alfonsussandy.id">alfonsussandy.id</a>
        <br/>
        <cite>- Alfonsus Sandy, Diporental</cite>
      </blockquote>
    </div>
  </div>
</div>

@endsection
