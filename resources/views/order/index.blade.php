@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Orders</div>
                <div class="panel-body">
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <div class="container-fluid">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!! Session::get('message') !!}
                            </div>
                        </div>
                        <br>
                    @endif

                    @if (!$orders->count())
                        <p class="text-info text-center">Belum ada Orders</p>
                    @else
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>Nama</td>
                                <td>Produk</td>
                                <td>Kontak</td>
                                <td>Status</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $order)
                                <tr style="{{ $order->done == 1 ? 'background-color:#dfffdf' : '' }}">
                                    <td>{{ $order->name }}</td>
                                    <td>
                                        Nama: {{ $order->product->title }} <br>
                                        Voucher: {{ $order->voucher ? $order->voucher->name : 'Tidak Ada' }}    
                                    </td>
                                    <td>
                                        Phone: {{ $order->phone }} <br>
                                        Email: {{ $order->email }} <br>
                                        Address: {{$order->address}}
                                    </td>
                                    <td>{{ $order->done == 0 ? 'Belum Selesai' : 'Selesai' }}</td>
                                    <td><a href="{{ url('order/done/'.$order->id) }}" class="btn btn-success btn-sm btn-block" {{ $order->done == 1 ? 'disabled' : '' }}>Selesai</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $orders->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

