@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Voucher</div>
                <div class="panel-body">
                    <form action="" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name">Nama</label>
                            <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                            {!! $errors->first('name', '<span class="help-block"><strong>:message</strong></span>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
                            <label for="amount">Diskon (%)</label>
                            <input type="number" name="amount" class="form-control" value="{{ old('amount') }}">
                            {!! $errors->first('amount', '<span class="help-block"><strong>:message</strong></span>') !!}
                        </div>
                        {{--  <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                            <label for="image">Image</label>
                            <input type="file" name="image" class="form-control">
                            {!! $errors->first('image', '<span class="help-block"><strong>:message</strong></span>') !!}
                        </div>  --}}
                        <div class="form-group {{ $errors->has('desc') ? 'has-error' : '' }}">
                            <label for="desc">Deskripsi</label>
                            <textarea class="form-control" name="desc" rows="2">{{ old('desc') }}</textarea>
                            {!! $errors->first('name', '<span class="help-block"><strong>:message</strong></span>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('start') ? 'has-error' : '' }}">
                            <label for="start">Mulai Promo</label>
                            <input type="date" name="start" class="form-control" value="{{ old('start') }}">
                            {!! $errors->first('start', '<span class="help-block"><strong>:message</strong></span>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('end') ? 'has-error' : '' }}">
                            <label for="end">Akhir Promo</label>
                            <input type="date" name="end" class="form-control" value="{{ old('end') }}">
                            {!! $errors->first('end', '<span class="help-block"><strong>:message</strong></span>') !!}
                        </div>
                        
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection