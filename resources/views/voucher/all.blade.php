@extends('layouts.front')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="text-center" style="margin-top: 30px">Kode Promo</h2>            
        </div>
        @if ($vouchers->count())
            @foreach ($vouchers as $voucher)
                <div class="col-4">
                    <div class="card card-order">
                        <img class="card-img-top img-fluid" src="{{ asset('images/disk.jpg') }}" alt="{{ $voucher->title }}">
                        <div class="card-body">
                            <p class="card-text">{{ str_limit($voucher->desc, 15) }}</p>
                            <p class="card-text"><b>Kode Promo:</b> {{ $voucher->name }}</p>
                            <p class="card-text"><b>Berlaku Sampai:</b> {{ date('d M Y', strtotime($voucher->end)) }}</p>
                            <a href="{{ url('voucher/show/'.$voucher->id) }}" class="btn btn-primary btn-block">Lihat Detail</a>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>
@endsection