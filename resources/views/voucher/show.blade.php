@extends('layouts.front')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-8">
            <div class="card card-order">
                <div class="card-body">
                    <h4 class="card-title">Promo {{ $voucher->name }}</h4>
                    <p class="card-text">Gunakan kode promo berikut untuk mendapatkan diskon sebesar <b>{{ $voucher->amount }}%</b></p>
                    <p class="card-text">Kode Promo: <b>{{ $voucher->name }}</b></p>
                    <p class="card-text">{{ $voucher->desc }}</p>
                    <hr>
                    <h3 class="card-title">Panduan Penggunaan</h3>
                    <p class="card-text">
                        <ol>
                            <li>Pilih promo yang ada pada halaman <a href="{{ url('voucher') }}">voucher</a>.</li>
                            <li>Catat kode promo yang tertera pada halaman tersebut.</li>
                            <li>Pilih template yang anda suka.</li>
                            <li>Pilih tombol 'Order Now' yang ada pada template.</li>
                            <li>Masukan data diri anda pada form yang disediakan dan jangan lupa masukan kode promo pada kolom voucher.</li>
                            <li>Selamat! anda berhasil mendapatkan diskon dari kami.</li>
                        </ol>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection