@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Vouchers</div>
                <div class="panel-body">
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <div class="container-fluid">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!! Session::get('message') !!}
                            </div>
                        </div>
                        <br>
                    @endif
                    <a href="{{ url('voucher/create') }}" class="btn btn-primary">Create Voucher</a>
                    <br><br>
                    @if (!$vouchers->count())
                        <p class="text-info text-center">Belum ada voucher</p>
                    @else
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>Nama</td>
                                <td>Deskripsi</td>
                                <td>Masa Berlaku</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($vouchers as $voucher)
                                <tr>
                                    <td>{{ $voucher->name }}</td>
                                    <td>
                                        Diskon: {{ $voucher->amount }}% <br>
                                        Desc: {{ str_limit($voucher->desc, 30) }}
                                    </td>
                                    <td>{{ $voucher->start.' - '.$voucher->end }}</td>
                                    <td><a href="{{ url('voucher/delete/'.$voucher->id) }}" class="btn btn-danger">Hapus</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection