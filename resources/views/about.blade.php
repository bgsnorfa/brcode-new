@extends('layouts.front')

@section('content')
<div class="paralax hero-about" style="height: 200px">
  <div class="hero-text">
    <h1>
      Tentang Kami
    </h1>
  </div>
</div>

<div class="container" style="margin-top:6em;margin-bottom:6em">
  <div class="row align-items-center">
    <div class="col-md-6">
        <h3>BR | Code - Marketplace Produk Digital</h3>
        <p>
          BR | Code adalah sebuah marketplace yang menjual produk digital kreatif, khususnya situs web.
        </p>
    </div>
    <div class="col-md-6">
      <img src="{{ asset('/images/macbook.png') }}" class="img-fluid" alt="macbook">
    </div>
  </div>
</div>

<div style="color:white; background-image: url({{ asset('/images/bulb.png') }}); background-repeat: no-repeat; background-size:cover;">
  <div class="container">
    <div class="row" style="padding-top:6em;padding-bottom:6em">
      <div class="col-md-6 ml-md-auto">
        <h3>Berikan Presentasi Menarik, Bangun Branding Terbaik</h3>
        <p>
          Ciptakan branding terbaik versimu sendiri.
          <br/>
          Berikan nuansa berbeda dan unik. Bukan hanya secara visual, tetapi juga secara emosional.
        </p>
      </div>
    </div>
  </div>
</div>

<div style="background:#298fd7">
<div class="container">
  <div class="row align-items-end">
    <div class="col-12">
      <h2 class="text-center text-light" style="margin-top:100px;margin-bottom:50px"><b>Formula Kami</b></h2>
    </div>
    <div class="col-md-3 text-center">
      <img src="{{ asset('images/dedikasi.png') }}" alt="Dedikasi" style="width:auto;max-height:150px">
      <h4 class="text-light" style="margin-top:20px;"><b>Dedikasi</b></h4>
    </div>
    <div class="col-md-1 text-center">
      <h3 class="text-light"><b>+</b></h3>
    </div>
    <div class="col-md-3 text-center">
      <div style="display:block;max-height: 150px;">
        <img src="{{ asset('images/kualitas.png') }}" alt="Kualitas" style="width:auto;max-height:150px">
      </div>
      <h4 class="text-light" style="margin-top:20px;"><b>Kualitas</b></h4>
    </div>
    <div class="col-md-1 text-center">
      <h3 class="text-light"><b>=</b></h3>
    </div>
    <div class="col-md-3 text-center">
      <img src="{{ asset('images/logo.jpg') }}" alt="Kualitas" class="img-fluid">
      <h4 class="text-light" style="margin-top:20px;"><b>BR | Code</b></h4>
    </div>
    <div class="col-12">
      <h5 class="text-center text-light" style="margin-top:70px;margin-bottom:6em;">BR|Code dibangun dengan berlandaskan dedikasi penuh untuk memberikan kualitas terbaik. Percayakan project anda kepada kami, dan bangun branding terbaik.</h5>
    </div>
  </div>
</div>
</div>
@endsection
