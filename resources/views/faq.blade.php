@extends('layouts.front')

@section('content')
<div class="paralax section section-faq">
  <div clas="hreo-text">
    <h1 class="text-center">FAQ</h1>
    <h3 class="text-center">Frequently Asked Questions</h3>
  </div>
</div>

<br/><br/>
<div class="container">
  <div class="row">
    <div class="col-12">
      <!-- 1 -->
      <button class="accordion">Apa jasa yang ditawarkan oleh BRCode?</button>
      <div class="panel">
        <p>
          Saat ini kami memberikan layanan berupa jasa pembuatan website, khususnya pembuatan company profile, personal resume atau Curriculum Vitae, dan website untuk organisasi
        </p>
      </div>

      <!-- 2 -->
      <button class="accordion">Bagaimana cara order jasa pembuatan web?</button>
      <div class="panel">
        <p>
          Untuk melakukan pemesanan/order, dapat menekan tombol ini <a class="btn btn-md btn-info" href="http://bit,ly.order-brcode">klik disini</a>
          <br/>
          Tombol tersebut akan mengarahkan pada komunikasi dengan admin melalui Whatsapp.
        </p>
      </div>

      <!-- 3 -->
      <button class="accordion">Bagaimana menentukan pilihan/desain untuk web saya?</button>
      <div class="panel">
        <p>
          Anda dapat berkonsultasi dengan kami melalui tombol ini <a href="http;//bit.ly/order-brcode" class="btn btn-md btn-info">klik disini</a>
          <br/>
          Dengan senang hati kami akan memberikan yang terbaik untuk anda.
        </p>
      </div>

      <!-- 4 -->
      <button class="accordion">Berapa lama waktu pengerjaan website pada umumnya?</button>
      <div class="panel">
        <p>
          Untuk rata-rata pengerjaan website adalah antara rentang 7 hari (1 minggu) sampai 30 hari (1 bulan)
        </p>
      </div>

      <!-- 5 -->
      <button class="accordion">Apa kelebihan yang ditawarkan oleh BRCode?</button>
      <div class="panel">
        <p>
          Dengan developer dan designer yang telah berpengalaman, kami memberikan kualitas terbaik untuk website anda dengan harga terbaik.<br/>
          Semua untuk menciptakan branding terbaik anda.
        </p>
      </div>

      <!-- 6 -->
      <button class="accordion">Apakah saya dapat memesan website secara custom?</button>
      <div class="panel">
        <p>
          Tentu saja :)
          <br/>
          Konsultasikan saja dengan kami apa kebutuhan anda, kami akan berikan pelayanan dan kualitas terbaik. Langsung saja <a class="btn btn-md btn-info" href="http://bit.ly/order-brcode">Klik disini</a> untuk mengkonsultasikan kebutuhan website anda.
        </p>
      </div>

    </div>
  </div>
</div>
<br/><br/>
@endsection
