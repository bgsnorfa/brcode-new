<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width,initial-scale=1"/>
  <title>
    <?php
    if (isset($title)) {
      echo $title . ' - ';
    }
    ?>
    BR | Code
  </title>

  <!-- favicon -->
  <link rel="icon" href="{{ asset('images/favicon.ico') }}"/>
  <!-- google font -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300&amp;subset=latin-ext" rel="stylesheet">
  <!-- bootstrap css -->
  <link rel="stylesheet" href="{{ asset('assets/dist/css/bootstrap.min.css') }}"/>
  <!-- font-awesome -->
  <link rel="stylesheet" href="{{ asset('assets/dist/font-awesome/css/font-awesome.min.css') }}"/>
  <!-- jasny css -->
  <link rel="stylesheet" href="{{ asset('assets/dist/jasny-bootstrap/css/jasny-bootstrap.min.css') }}"/>
  <!-- owl carousel -->
  <link rel="stylesheet" href="{{ asset('assets/dist/css/owl.carousel.min.css') }}"/>
  <link rel="stylesheet" href="{{ asset('assets/dist/css/owl.theme.default.min.css') }}"/>
  <!-- custom css -->
  <link rel="stylesheet" href="{{ asset('assets/custom/css/front.css') }}"/>
</head>

<body>

  <nav class="navbar navbar-expand-sm navbar-light fixed-top header">

    <div class="container">

      <button class="navbar-toggler" type="button" data-toggle="offcanvas" data-target=".navmenu" aria-expanded="false" data-canvas="body" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Brand -->
      <a class="navbar-brand" href="{{ url('/') }}">
        <img src="{{ asset('images/logo.jpg') }}" class="aligned-top" height="50"/>
      </a>

      <!-- Links -->
      <div class="collapse navbar-collapse justify-content-end" id="nav-content">
        <ul class="navbar-nav">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuProduct" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Produk</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuProduct">
                <a class="dropdown-item" href="{{ url('/cv') }}">Personal Resume</a>
                <a class="dropdown-item" href="{{ url('/blog') }}">Blog</a>
                <a class="dropdown-item" href="{{ url('/business') }}">Corporate Business</a>
                <a class="dropdown-item" href="http://bit.ly/order-brcode">Custom Web</a>
            </div>
          </li>
          <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuAff" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
          Affiliasi
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuAff">
          <a class="dropdown-item" href="#">Pelajari lebih lanjut</a>
          <a class="dropdown-item" href="#">Login</a>
          <a class="dropdown-item" href="#">Daftar</a>
        </div>
      </li> -->
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/tentang-kami') }}">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/faq') }}">FAQ</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- push menu -->
  <div class="navmenu navmenu-inverse navmenu-fixed-left offcanvas">
    <div class="container">
      <a class="navmenu-brand text-center close-menu" data-target=".navmenu" data-toggle="offcanvas" href="#">CLOSE (&times;)</a>
      <ul class="push-ul navmenu-nav">
        <li class="nav-item">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuProductPush" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Produk</a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuProductPush">
              <a class="dropdown-item" href="{{ url('/cv') }}">Personal Resume</a>
              <a class="dropdown-item" href="{{ url('/blog') }}">Blog</a>
              <a class="dropdown-item" href="{{ url('/business') }}">Corporate Business</a>
              <a class="dropdown-item" href="http://bit.ly/order-brcode">Custom Web</a>
          </div>
        </li>

        <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuAffPush" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Affiliasi
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuAffPush">
          <a class="dropdown-item" href="#">Pelajari lebih lanjut</a>
          <a class="dropdown-item" href="#">login</a>
          <a class="dropdown-item" href="#">Daftar</a>
        </div>
      </li> -->

        <li class="nav-item">
          <a class="nav-link" href="{{ url('/tentang-kami') }}">Tentang</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/faq') }}">FAQ</a>
        </li>
      </ul>
    </div>
  </div>

  @yield('content')

  <div class="section-footer">
    <div class="container">


        <div class="row" style="border-bottom: 1px solid grey;">
          <div class="col-sm-6 text-center">
            <h1>10</h1>
            <p>Klien telah bekerja sama dengan kami</p>
          </div>

          <div class="col-sm-6 text-center">
            <h1>21</h1>
            <p>Produk digital telah kami hasilkan</p>
          </div>

          <!-- <div class="col-sm-4 text-center">
            <h1></h1>
            <p></p>
          </div> -->

        </div>
        <br/>

        <div class="row">
            <div class="col-6 col-sm-4 col-md-4">
              <p class="p-address">
                Gedung ICT lt. 2 Kampus Undip,
                <br/>
                Jl. Prof Soedharto, SH,
                <br/>
                Tembalang - Semarang
                <br/><br/>
                Phone : <a class="p-link" href="tel:+6282138376922">+62-821-3837-6922</a>
                <br/>
                Mail : <a class="p-link" href="mailto:mail@brcode.id">mail@brcode.id</a>
              </p>
            </div>

            <div class="col-6 col-sm-4 col-md-4">
              <p class="p-address">Find Us</p>
              <br/>
              <a class="fa-contact p-link" href="https://instagram.com/brcodeid" target="_BLANK"><i class="fa fa-instagram"></i></a>
              <a class="fa-contact p-link" href="#" target="_BLANK"><i class="fa fa-facebook"></i></a>
            </div>

            <div class="col-sm-4 col-md-4">
              <p class="p-address">BR | Code</p>
              <a class="p-link" href="{{ url('/tentang-kami') }}">Tentang Kami</a>
              <br/>
              <a class="p-link" href="{{ url('/faq') }}">FAQ</a>
            </div>
        </div>

    </div>
  </div>
  <footer style="background-color: #212121; border-top: 1px dashed #2f2f2f; color: #eaeaea; padding-top: 5px; padding-bottom: 5px; font-size: 90%;">
    <div class="container">
      &copy; BR | Code - {{ date('Y') }}
    </div>
  </footer>

  <!-- jquery js -->
  <script src="{{ asset('assets/dist/jquery/jquery-3.2.1.min.js') }}"></script>
  <!-- popper js -->
  <script src="{{ asset('assets/dist/js/popper.min.js') }}"></script>
  <!-- bootstrap js -->
  <script src="{{ asset('assets/dist/js/bootstrap.min.js') }}"></script>
  <!-- jasny js -->
  <script src="{{ asset('assets/dist/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"></script>
  <!-- owl js -->
  <script src="{{ asset('assets/dist/js/owl.carousel.min.js') }}"></script>
  <!-- custom js -->
  <script src="{{ asset('assets/custom/js/front.js') }}"></script>
  @yield('bottom-script')
</body>
</html>
