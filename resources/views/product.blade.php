@extends('layouts.front')

@section('content')

<div class="paralax section section-productPage" style="height: 250px">
    <div class="hero-text">
        <h1 class="text-center">Produk Kami</h1>
    </div>
</div>

<div class="section container">
    <div class="row">
        <div class="col-12 text-center">
            <h2>Temukan Berbagai Tampilan yang Sesuai dengan Kebutuhan Anda </h2>
        </div>
    </div>
    <br/><br/><br/>

    <!-- cv -->
    <div class="row" style="margin-bottom: 3em;">
        <div class="col-12 text-center">
            <h3 style="color: grey;">Personal Resume (Curriculum Vitae)</h3>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="#">
                    <img src="{{ asset('images/sandy.jpg') }}" class="img-fluid"/>
                    <div class="price-topleft-blue">
                        <p>IDR 500K</p>
                    </div>
                </a>
            </div>

            <div class="text-center">
                <a href="#" class="product-link">
                    <h4>Disini Link</h4>
                </a>
                <p>By <a href="#" class="product-link">envato</a></p>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="#">
                    <img src="{{ asset('images/sandy.jpg') }}" class="img-fluid"/>
                    <div class="price-topleft-blue">
                        <p>IDR 500K</p>
                    </div>
                </a>
            </div>

            <div class="text-center">
                <a href="#" class="product-link">
                    <h4>Disini Link</h4>
                </a>
                <p>By <a href="#" class="product-link">envato</a></p>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="#">
                    <img src="{{ asset('images/sandy.jpg') }}" class="img-fluid"/>
                    <div class="price-topleft-blue">
                        <p>IDR 500K</p>
                    </div>
                </a>
            </div>

            <div class="text-center">
                <a href="#" class="product-link">
                    <h4>Disini Link</h4>
                </a>
                <p>By <a href="#" class="product-link">envato</a></p>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="#">
                    <img src="{{ asset('images/sandy.jpg') }}" class="img-fluid"/>
                    <div class="price-topleft-blue">
                        <p>IDR 500K</p>
                    </div>
                </a>
            </div>

            <div class="text-center">
                <a href="#" class="product-link">
                    <h4>Disini Link</h4>
                </a>
                <p>By <a href="#" class="product-link">envato</a></p>
            </div>
        </div>

        <div class="col-12 text-center">
            <a class="btn btn-info" href="#">Lihat tampilan CV Selengkapnya <i class="fa fa-chevron-right"></i></a>
        </div>

    </div>

    <!-- blog -->
    <div class="row" style="margin-bottom: 3em;">
        <div class="col-12 text-center">
            <h2 style="color: grey;">Blog</h2>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="#">
                    <img src="{{ asset('images/sandy.jpg') }}" class="img-fluid"/>
                    <div class="price-topleft-green">
                        <p>IDR 500K</p>
                    </div>
                </a>
            </div>

            <div class="text-center">
                <a href="#" class="product-link">
                    <h4>Disini Link</h4>
                </a>
                <p>By <a href="#" class="product-link">envato</a></p>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="#">
                    <img src="{{ asset('images/sandy.jpg') }}" class="img-fluid"/>
                    <div class="price-topleft-green">
                        <p>IDR 500K</p>
                    </div>
                </a>
            </div>

            <div class="text-center">
                <a href="#" class="product-link">
                    <h4>Disini Link</h4>
                </a>
                <p>By <a href="#" class="product-link">envato</a></p>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="#">
                    <img src="{{ asset('images/sandy.jpg') }}" class="img-fluid"/>
                    <div class="price-topleft-green">
                        <p>IDR 500K</p>
                    </div>
                </a>
            </div>

            <div class="text-center">
                <a href="#" class="product-link">
                    <h4>Disini Link</h4>
                </a>
                <p>By <a href="#" class="product-link">envato</a></p>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="#">
                    <img src="{{ asset('images/sandy.jpg') }}" class="img-fluid"/>
                    <div class="price-topleft-green">
                        <p>IDR 500K</p>
                    </div>
                </a>
            </div>

            <div class="text-center">
                <a href="#" class="product-link">
                    <h4>Disini Link</h4>
                </a>
                <p>By <a href="#" class="product-link">envato</a></p>
            </div>
        </div>

        <div class="col-12 text-center">
            <a class="btn btn-info" href="#">Lihat tampilan Blog Selengkapnya <i class="fa fa-chevron-right"></i></a>
        </div>

    </div>

    <!-- corporate business -->
    <div class="row" style="margin-bottom: 3em;">
        <div class="col-12 text-center">
            <h2 style="color: grey;">Corporate Business</h2>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="#">
                    <img src="{{ asset('images/sandy.jpg') }}" class="img-fluid"/>
                    <div class="price-topleft-red">
                        <p>IDR 500K</p>
                    </div>
                </a>
            </div>

            <div class="text-center">
                <a href="#" class="product-link">
                    <h4>Disini Link</h4>
                </a>
                <p>By <a href="#" class="product-link">envato</a></p>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="#">
                    <img src="{{ asset('images/sandy.jpg') }}" class="img-fluid"/>
                    <div class="price-topleft-red">
                        <p>IDR 500K</p>
                    </div>
                </a>
            </div>

            <div class="text-center">
                <a href="#" class="product-link">
                    <h4>Disini Link</h4>
                </a>
                <p>By <a href="#" class="product-link">envato</a></p>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="#">
                    <img src="{{ asset('images/sandy.jpg') }}" class="img-fluid"/>
                    <div class="price-topleft-red">
                        <p>IDR 500K</p>
                    </div>
                </a>
            </div>

            <div class="text-center">
                <a href="#" class="product-link">
                    <h4>Disini Link</h4>
                </a>
                <p>By <a href="#" class="product-link">envato</a></p>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="grid-product">
                <a href="#">
                    <img src="{{ asset('images/sandy.jpg') }}" class="img-fluid"/>
                    <div class="price-topleft-red">
                        <p>IDR 500K</p>
                    </div>
                </a>
            </div>

            <div class="text-center">
                <a href="#" class="product-link">
                    <h4>Disini Link</h4>
                </a>
                <p>By <a href="#" class="product-link">envato</a></p>
            </div>
        </div>

        <div class="col-12 text-center">
            <a class="btn btn-info" href="#">Lihat tampilan Korporasi Selengkapnya <i class="fa fa-chevron-right"></i></a>
        </div>

    </div>

</div>

@endsection
