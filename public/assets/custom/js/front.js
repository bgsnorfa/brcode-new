// script to config owl carousel in front page
$(document).ready(function () {
  $('.owl-carousel').owlCarousel({
    loop: 'false',
    dotsEach: 'true',
    autoplay: 'true',
    autoplayTimeout: '2000',
    autoplayHoverPause: 'true'
  });
});


// script for accordion
var acc = document.getElementsByClassName("accordion");
var i;

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  }
}
